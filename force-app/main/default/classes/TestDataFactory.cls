@isTest
public with sharing class TestDataFactory {

    private static Integer MAX_AMOUNT_VALUE = 100;

    public static List<Contact> createContactsWithXYAmounts(Integer numOfContacts, List<Account> accs)
    {
        List<Contact> generatedCons = new List<Contact>();

        for(Integer i = 0; i < numOfContacts; i++)
        {
            Contact newCon = new Contact();
            newCon.LastName = 'TestCon' + i;
            newCon.Type__c = getRandomType();
            newCon.Amount_X__c = getRandomAmount();
            newCon.Amount_Y__c = getRandomAmount();
            newCon.AccountId = accs[getRandomInteger(accs.size() - 1)].Id;

            generatedCons.add(newCon);
        }
        insert generatedCons;

        System.debug('Inserted cons size: ' + generatedCons.size());

        return generatedCons;
    }

    public static List<Account> createAccounts(Integer numOfAccounts)
    {
        List<Account> generatedAccs = new List<Account>();
        for(Integer i = 0; i < numOfAccounts; i++)
        {
            generatedAccs.add(new Account(Name='TestAcc' + i));   
        }
        insert generatedAccs;

        System.debug('Inserted accs size: ' + generatedAccs.size());

        return generatedAccs;
    }

    public static void createAccountsWithContactsWithXYAmounts(Integer numOfAccounts, Integer numOfContacts)
    {
        System.debug('Accs to create: ' + numOfAccounts + '  Con to create: ' + numOfContacts);
        List<Account> generatedAccs = TestDataFactory.createAccounts(numOfAccounts);
        List<Contact> generatedCons = TestDataFactory.createContactsWithXYAmounts(numOfContacts, generatedAccs);
    }

    private static Integer getRandomInteger(Integer max)
    {
        return Integer.valueOf(Math.random() * max);
    }

    private static String getRandomType()
    {
        return getRandomInteger(100) > 50 ? 'Positive' : 'Negative';
    }

    private static Decimal getRandomAmount()
    {
        return Math.random() * MAX_AMOUNT_VALUE;
    }
}
