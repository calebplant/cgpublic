@isTest
private class ContactTrigger_Test {

    // ==== Update these if you change the @TestSetup data ====
    private static Decimal ACC_1_START_ROLLUP_X = 30;
    private static Decimal ACC_1_START_ROLLUP_Y = 7;
    private static Decimal ACC_1_START_ROLLUP_TOTAL = ACC_1_START_ROLLUP_X + ACC_1_START_ROLLUP_Y;
    private static Decimal ACC_2_START_ROLLUP_X = 1;
    private static Decimal ACC_2_START_ROLLUP_Y = 0;
    private static Decimal ACC_2_START_ROLLUP_TOTAL = ACC_2_START_ROLLUP_X + ACC_2_START_ROLLUP_Y;
    private static String ACC_1_NAME = 'TestAcc1';
    private static String ACC_2_NAME = 'TestAcc2';
    
    private static Integer BULK_SIZE = 100;
    private static Integer FIRST_N_SUM = BULK_SIZE * (BULK_SIZE+1)/2; // sum = n(n+1)/2... ex: n=100 => sum=5050

    
    @TestSetup
    static void makeData(){

        List<Account> accs = new List<Account>();
        accs.add(new Account(Name=ACC_1_NAME));
        accs.add(new Account(Name=ACC_2_NAME));
        insert accs;

        List<Contact> cons = new List<Contact>();
        // LastName = TestCon[Type__c]_[Amount_n__c]
        // Acc 1
        cons.add(new Contact(LastName='TestConP_10', AccountId=accs[0].Id, Amount_X__c=10, Amount_Y__c=1, Type__c='Positive'));
        cons.add(new Contact(LastName='TestConP_20', AccountId=accs[0].Id, Amount_X__c=20, Amount_Y__c=2, Type__c='Positive'));
        cons.add(new Contact(LastName='TestConN_3', AccountId=accs[0].Id, Amount_X__c=30, Amount_Y__c=3, Type__c='Negative'));
        cons.add(new Contact(LastName='TestConN_4', AccountId=accs[0].Id, Amount_X__c=0, Amount_Y__c=4, Type__c='Negative'));
        // Acc 2
        cons.add(new Contact(LastName='TestConP_0', AccountId=accs[1].Id, Amount_X__c=0, Amount_Y__c=10, Type__c='Positive'));
        cons.add(new Contact(LastName='TestConP_1', AccountId=accs[1].Id, Amount_X__c=1, Amount_Y__c=0,  Type__c='Positive'));
        insert cons;
    }

    @isTest
    static void doesTriggerRollupContactsOnInsert_BULK()
    {
        System.debug('ContactTrigger_Test :: doesTriggerRollupContactsOnInsert_BULK');

        // Ensure start values
        Account acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        Account acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];
        ContactTrigger_Test.checkStartValues(acc1, acc2);

        // Create contacts to insert
        List<Contact> insertedCons = new List<Contact>();
        for(Integer i = 1; i < BULK_SIZE+1; i++)
        {
            insertedCons.add(new Contact(LastName='BulkCon'+i, AccountId=acc1.Id, Amount_X__c=i, Amount_Y__c=0, Type__c='Positive'));
        }

        // Insert cons
        Test.startTest();
        insert insertedCons;
        Test.stopTest();

        // Get updated account info
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];

        // Check X Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X + FIRST_N_SUM, acc1.Rollup_Amount_X__c);
        // Check Y Rollup
        System.assertEquals(ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount_Y__c);
        // Check total Rollup
        System.assertEquals((ACC_1_START_ROLLUP_X + FIRST_N_SUM) + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c);
    }

    @isTest
    static void doesTriggerRollupContactsOnInsert_SMALL()
    {
        System.debug('ContactTrigger_Test :: doesTriggerRollupContactsOnInsert_SMALL');

        // Ensure start values
        Account acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        Account acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];
        ContactTrigger_Test.checkStartValues(acc1, acc2);

        // Setup contacts to insert
        List<Contact> insertedCons = new List<Contact>();
        insertedCons.add(new Contact(LastName='insertCon1', AccountId=acc1.Id, Amount_X__c=2, Amount_Y__c=6, Type__c='Positive'));
        insertedCons.add(new Contact(LastName='insertCon2', AccountId=acc2.Id, Amount_X__c=4, Amount_Y__c=8, Type__c='Negative'));

        // Insert cons
        Test.startTest();
        insert insertedCons;
        Test.stopTest();

        // Get updated account info
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];

        // Check X Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X + 2, acc1.Rollup_Amount_X__c);
        System.assertEquals(ACC_2_START_ROLLUP_X + 0, acc2.Rollup_Amount_X__c);
        // Check Y Rollup
        System.assertEquals(ACC_1_START_ROLLUP_Y + 0, acc1.Rollup_Amount_Y__c);
        System.assertEquals(ACC_2_START_ROLLUP_Y + 8, acc2.Rollup_Amount_Y__c);
        // Check total Rollup
        System.assertEquals((ACC_1_START_ROLLUP_X + 2) + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c);
        System.assertEquals(ACC_2_START_ROLLUP_X + (ACC_2_START_ROLLUP_Y + 8), acc2.Rollup_Amount__c);
    }

    @isTest
    static void doesTriggerRollupContactsOnUpdate_BULK()
    {
        System.debug('ContactTrigger_Test :: doesTriggerRollupContactsOnUpdate_BULK');

        // Ensure start values
        Account acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        Account acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];
        ContactTrigger_Test.checkStartValues(acc1, acc2);

        // Insert bulk contacts
        List<Contact> insertedCons = new List<Contact>();
        for(Integer i = 1; i < BULK_SIZE+1; i++)
        {
            insertedCons.add(new Contact(LastName='BulkCon'+i, AccountId=acc1.Id, Amount_X__c=i, Amount_Y__c=0, Type__c='Positive'));
        }
        insert insertedCons;

        // Set up contacts to update (increment their amount by 1)
        List<Contact> updatedCons = [SELECT Id, Amount_X__c FROM Contact WHERE Name LIKE 'BulkCon%'];
        for(Contact eachCon : updatedCons)
        {
            eachCon.Amount_X__c += 1;
        }

        // Update cons
        Test.startTest();
        update updatedCons;
        Test.stopTest();

        // Get updated account info
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];

        // Check X Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X + FIRST_N_SUM + BULK_SIZE, acc1.Rollup_Amount_X__c);
        // Check Y Rollup
        System.assertEquals(ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount_Y__c);
        // Check total Rollup
        System.assertEquals((ACC_1_START_ROLLUP_X + FIRST_N_SUM + BULK_SIZE) + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c);
    }

    @isTest
    static void doesTriggerRollupContactsOnUpdate_SMALL()
    {
        System.debug('ContactTrigger_Test :: doesTriggerRollupContactsOnUpdate_SMALL');

        // Ensure start values
        Account acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        Account acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];
        ContactTrigger_Test.checkStartValues(acc1, acc2);

        // Get contacts to update
        Contact acc1Con = [SELECT Id, Name, Amount_X__c, Amount_Y__c, Type__c FROM Contact WHERE Name='TestConP_10' LIMIT 1];
        Contact acc2Con = [SELECT Id, Name, Amount_X__c, Amount_Y__c, Type__c FROM Contact WHERE Name='TestConP_0' LIMIT 1];
        // Update values
        acc1Con.Amount_X__c = 0; // start value: 10
        acc2Con.Amount_Y__c = 20;
        acc2Con.Type__c = 'Negative';

        // Update cons
        Test.startTest();
        List<Contact> updatedCons = new List<Contact>{acc1Con, acc2Con};
        update updatedCons;
        Test.stopTest();

        // Get updated account info
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];

        // Check X Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X - 10, acc1.Rollup_Amount_X__c);
        System.assertEquals(ACC_2_START_ROLLUP_X + 0, acc2.Rollup_Amount_X__c);
        // Check Y Rollup
        System.assertEquals(ACC_1_START_ROLLUP_Y + 0, acc1.Rollup_Amount_Y__c);
        System.assertEquals(ACC_2_START_ROLLUP_Y + 20, acc2.Rollup_Amount_Y__c);
        // Check total Rollup
        System.assertEquals((ACC_1_START_ROLLUP_X - 10) + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c);
        System.assertEquals(ACC_2_START_ROLLUP_X + (ACC_2_START_ROLLUP_Y + 20), acc2.Rollup_Amount__c);
    }

    @isTest
    static void doesTriggerRollupContactsOnDelete_BULK()
    {
        System.debug('ContactTrigger_Test :: doesTriggerRollupContactsOnDelete_BULK');

        Account acc1 = [SELECT Id FROM Account WHERE Name = :ACC_1_NAME];

        // Insert bulk contacts
        List<Contact> insertedCons = new List<Contact>();
        for(Integer i = 1; i < BULK_SIZE+1; i++)
        {
            insertedCons.add(new Contact(LastName='BulkCon'+i, AccountId=acc1.Id, Amount_X__c=i, Amount_Y__c=0, Type__c='Positive'));
        }
        insert insertedCons;
        
        // Ensure start values
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        System.assertEquals(ACC_1_START_ROLLUP_X + FIRST_N_SUM, acc1.Rollup_Amount_X__c); // X
        System.assertEquals(ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount_Y__c); // Y
        System.assertEquals((ACC_1_START_ROLLUP_X + FIRST_N_SUM) + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c); // total

        // Set up contacts to delete
        List<Contact> deletedCons = [SELECT Id, Amount_X__c FROM Contact WHERE Name LIKE 'BulkCon%'];

        // Delete cons
        Test.startTest();
        delete deletedCons;
        Test.stopTest();

        // Get updated account info
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];

        // Check X Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X, acc1.Rollup_Amount_X__c);
        // Check Y Rollup
        System.assertEquals(ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount_Y__c);
        // Check total Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c);
    }

    @isTest
    static void doesTriggerRollupContactsOnDelete_SMALL()
    {
        System.debug('ContactTrigger_Test :: doesTriggerRollupContactsOnDelete_SMALL');

        // Ensure start values
        Account acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        Account acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];
        ContactTrigger_Test.checkStartValues(acc1, acc2);

        // Get contacts to delete
        List<Contact> deletedCons = new List<Contact>();
        deletedCons.add([SELECT Id FROM Contact WHERE Name='TestConP_10' LIMIT 1]); // acc1
        deletedCons.add([SELECT Id FROM Contact WHERE Name='TestConP_0' LIMIT 1]); // acc2        

        // Delete cons
        Test.startTest();
        delete deletedCons;
        Test.stopTest();

        // Get updated account info
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];

        // Check X Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X - 10, acc1.Rollup_Amount_X__c);
        System.assertEquals(ACC_2_START_ROLLUP_X, acc2.Rollup_Amount_X__c);
        // Check Y Rollup
        System.assertEquals(ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount_Y__c);
        System.assertEquals(ACC_2_START_ROLLUP_Y, acc2.Rollup_Amount_Y__c);
        // Check total Rollup
        System.assertEquals((ACC_1_START_ROLLUP_X - 10) + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c);
        System.assertEquals(ACC_2_START_ROLLUP_X + (ACC_2_START_ROLLUP_Y), acc2.Rollup_Amount__c);
    }

    @isTest
    static void doesTriggerRollupContactsOnUnDelete_BULK()
    {
        System.debug('ContactTrigger_Test :: doesTriggerRollupContactsOnUnDelete_BULK');

        Account acc1 = [SELECT Id FROM Account WHERE Name = :ACC_1_NAME];

        // Insert bulk contacts
        List<Contact> insertedCons = new List<Contact>();
        for(Integer i = 1; i < BULK_SIZE+1; i++)
        {
            insertedCons.add(new Contact(LastName='BulkCon'+i, AccountId=acc1.Id, Amount_X__c=i, Amount_Y__c=0, Type__c='Positive'));
        }
        insert insertedCons;

        // Delete inserted contacts
        List<Contact> deletedCons = [SELECT Id, Amount_X__c FROM Contact WHERE Name LIKE 'BulkCon%'];
        delete deletedCons;

        // Get contacts to undelete
        List<Contact> undeletedCons = [SELECT Id, Amount_X__c FROM Contact WHERE IsDeleted = True ALL ROWS];

        // Undelete cons
        Test.startTest();
        undelete undeletedCons;
        Test.stopTest();

        // Get updated account info
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];

        // Check X Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X + FIRST_N_SUM, acc1.Rollup_Amount_X__c);
        // Check Y Rollup
        System.assertEquals(ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount_Y__c);
        // Check total Rollup
        System.assertEquals((ACC_1_START_ROLLUP_X + FIRST_N_SUM) + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c);
    }

    @isTest
    static void doesTriggerRollupContactsOnUnDelete_SMALL()
    {
        System.debug('ContactTrigger_Test :: doesTriggerRollupContactsOnUnDelete_SMALL');

        // Delete contacts
        List<Contact> deletedCons = new List<Contact>();
        deletedCons.add([SELECT Id FROM Contact WHERE Name='TestConP_10' LIMIT 1]); // acc1
        deletedCons.add([SELECT Id FROM Contact WHERE Name='TestConP_1' LIMIT 1]); // acc2  
        delete deletedCons;

        // Ensure values were deleted values
        Account acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        Account acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];
        System.assertNotEquals(ACC_1_START_ROLLUP_X, acc1.Rollup_Amount_X__c);
        System.assertNotEquals(ACC_2_START_ROLLUP_X, acc2.Rollup_Amount_X__c);

        // Get cons to undelete
        List<Contact> undeletedCons = [SELECT Id FROM Contact WHERE IsDeleted = True ALL ROWS];
    
        // Undelete cons
        Test.startTest();
        undelete undeletedCons;
        Test.stopTest();

        // Get updated account info
        acc1 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_1_NAME LIMIT 1];
        acc2 = [SELECT Id, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                        FROM Account WHERE Name = :ACC_2_NAME LIMIT 1];

        // Check X Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X, acc1.Rollup_Amount_X__c);
        System.assertEquals(ACC_2_START_ROLLUP_X, acc2.Rollup_Amount_X__c);
        // Check Y Rollup
        System.assertEquals(ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount_Y__c);
        System.assertEquals(ACC_2_START_ROLLUP_Y, acc2.Rollup_Amount_Y__c);
        // Check total Rollup
        System.assertEquals(ACC_1_START_ROLLUP_X + ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount__c);
        System.assertEquals(ACC_2_START_ROLLUP_X + ACC_2_START_ROLLUP_Y, acc2.Rollup_Amount__c);
    }

    private static void checkStartValues(Account acc1, Account acc2)
    {
        System.assertEquals(ACC_1_START_ROLLUP_X, acc1.Rollup_Amount_X__c);
        System.assertEquals(ACC_2_START_ROLLUP_X, acc2.Rollup_Amount_X__c);
        System.assertEquals(ACC_1_START_ROLLUP_Y, acc1.Rollup_Amount_Y__c);
        System.assertEquals(ACC_2_START_ROLLUP_Y, acc2.Rollup_Amount_Y__c);
        System.assertEquals(ACC_1_START_ROLLUP_TOTAL, acc1.Rollup_Amount__c);
        System.assertEquals(ACC_2_START_ROLLUP_TOTAL, acc2.Rollup_Amount__c);
    }
}
