@isTest
private class TestDataFactory_Test {
    
    @isTest
    static void doesCreateAccountsWithContactsWithXYAmountsCreateAccountsAndContacts()
    {
        Integer numOfAccounts = 10;
        Integer numOfContacts = 100;
        TestDataFactory.createAccountsWithContactsWithXYAmounts(numOfAccounts, numOfContacts);

        Integer generatedAccountSize = [SELECT Count() FROM Account];
        Integer generatedContactSize = [SELECT Count() FROM Contact];
        System.assertEquals(numOfAccounts, generatedAccountSize);
        System.assertEquals(numOfContacts, generatedContactSize);

        // Does generated account have Amount_X, Amount_Y, Account lookup?
        Contact someContact = [SELECT Id, Amount_x__c, Amount_Y__c, AccountId FROM Contact LIMIT 1];
        System.assert(someContact.Amount_X__c != 0);
        System.assert(someContact.Amount_Y__c != 0);
        System.assert(someContact.AccountId != null);
    }
}
