public without sharing class ContactTrigger_Helper {
    public static void updateAmounts(List<Contact> triggerCons)
    {
        System.debug('ContactTrigger_Helper :: updateAmounts');
        Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

        // Get set of Accounts to update
        Set<Id> accIds = new Set<Id>();
        for(Contact eachCon : triggerCons) {
            accIds.add(eachCon.AccountId);
        }

        try {
            // Map Accounts to update by their Id
            accountsToUpdate = new Map<Id, Account>(
                [SELECT Id, Name, Rollup_Amount_X__c, Rollup_Amount_Y__c, Rollup_Amount__c
                  FROM Account
                  WHERE Id IN :accIds]);
        } catch (Exception e) {
            System.debug('Exception: ' + e.getMessage());
        }

        try {
            // Get sum of existing contacts' amounts
            AggregateResult[] existingXAmounts = [SELECT AccountId, SUM(Amount_X__c) xSum FROM Contact
                                                WHERE Type__c = 'Positive' AND AccountId IN :accIds
                                                GROUP BY AccountId];
            AggregateResult[] existingYAmounts = [SELECT AccountId, SUM(Amount_Y__c) ySum FROM Contact
                                                WHERE Type__c = 'Negative' AND AccountId IN :accIds
                                                GROUP BY AccountId];

            // Update Rollup_X_Amount
            for(AggregateResult ar : existingXAmounts) {
                Id currentAccId = String.valueOf(ar.get('AccountId'));
                accountsToUpdate.get(currentAccId).Rollup_Amount_X__c = (Decimal)ar.get('xSum');
            }
            // Update Rollup_Y_Amount
            for(AggregateResult ar : existingYAmounts) {
                Id currentAccId = String.valueOf(ar.get('AccountId'));
                accountsToUpdate.get(currentAccId).Rollup_Amount_Y__c = (Decimal)ar.get('ySum');
            }

            // Update Rollup_Amount
            for(Account eachAcc : accountsToUpdate.values()) {
                eachAcc.Rollup_Amount__c = eachAcc.Rollup_Amount_X__c + eachAcc.Rollup_Amount_Y__c;
            }
        } catch (Exception e) {
            System.debug('Exception: ' + e.getMessage());
        }

        try {
            // DML statement
            if (accountsToUpdate.size() > 0) {
                update accountsToUpdate.values();
            }
        } catch (Exception e) {
            System.debug('Exception: ' + e.getMessage());
        }
        
    }
}
