trigger ContactTrigger on Contact (after insert, after update, after delete, after undelete) {

    if(Trigger.isAfter)
    {
        if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete)
        {
            ContactTrigger_Helper.updateAmounts(Trigger.new);
        }
        else if (Trigger.isDelete)
        {
            ContactTrigger_Helper.updateAmounts(Trigger.old);
        }
    }
}